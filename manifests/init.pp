class nli_pr (
  $ruby_version = $nli_pr::params::ruby_version,
  $deploy_dir = $nli_pr::params::deploy_dir,
  $deploy_user =$nli_pr::params::deploy_user,
  $deploy_group = $nli_pr::params::deploy_group,
  $blacklight_secret = $nli_pr::params::blacklight_secret,
  $solr_host = $nli_pr::params::solr_host,
  $solr_port = $nli_pr::params::solr_port,
  $solr_cores = $nli_pr::params::solr_cores,
  $pr_mapbox_access_token = $nli_pr::params::pr_mapbox_access_token,
  $pr_mapbox_county_map = $nli_pr::params::pr_mapbox_county_map,
  $pr_mapbox_diocese_map = $nli_pr::params::pr_mapbox_diocese_map,
  $pr_mapbox_city_map = $nli_pr::params::pr_mapbox_city_map,
  $pr_local_county_map = $nli_pr::params::pr_local_county_map,
  $pr_local_diocese_map = $nli_pr::params::pr_local_diocese_map,
  $pr_local_city_map = $nli_pr::params::pr_local_city_map,
  $pr_map_server_type = $nli_pr::params::pr_map_server_type,
  $pr_image_server_ip = $nli_pr::params::pr_image_server_ip,
  $pr_image_server_fcgi_path = $nli_pr::params::pr_image_server_fcgi_path,
  $pr_divaserve_ip = $nli_pr::params::pr_divaserve_ip,
  $pr_divaserve_path = $nli_pr::params::pr_divaserve_path,
  $pr_image_url_path = $nli_pr::params::pr_image_url_path,
  $pr_use_static_jpgs = $nli_pr::params::pr_use_static_jpgs,
  $pr_feedback_email = $nli_pr::params::pr_feedback_email,
  $pr_perform_caching = $nli_pr::params::pr_perform_caching,
  $pr_cache_store     = $nli_pr::params::pr_cache_store,
  $auth_enabled = $nli_pr::params::auth_enabled,
  $auth_user_file = $nli_pr::params::auth_user_file,
  $auth_user_name = $nli_pr::params::auth_user_name,
  $auth_user_pass = $nli_pr::params::auth_user_pass,
  $apache_servername = $nli_pr::params::apache_servername,
  $apache_aliases = $nli_pr::params::apache_aliases,
  $apache_port = $nli_pr::params::apache_port,
  ) inherits nli_pr::params {

  #### passenger and dependencies
  $required_packages = ['gcc-c++', 'ruby-devel', 'apr-devel', 'apr-util-devel', 'unzip']

  # Using puppetlabs-passenger because mod_passenger is not yet available from epel.
  # TODO: revert to using normal apache module once mod_passenger is available.
  class {'passenger':
    package_ensure         => '4.0.59',
    passenger_version      => '4.0.59',
    passenger_provider     => 'gem',
    passenger_package      => 'passenger',
    gem_path               => '/usr/local/share/gems/gems',
    gem_binary_path        => '/usr/local/bin',
    passenger_root         => '/usr/local/share/gems/gems/passenger-4.0.59',
    passenger_ruby         => "/usr/local/rvm/gems/$ruby_version/wrappers/ruby",
    passenger_app_env  =>  $environment,
    mod_passenger_location => '/usr/local/share/gems/gems/passenger-4.0.59/buildout/apache2/mod_passenger.so',
    require                => [Package[$required_packages], Apache::Vhost[$apache_servername]],
  }

  package {$required_packages:
    ensure => 'installed',
  }

  #### selinux
  class { 'selinux':
    mode => 'permissive'
  }

  #### ruby and dependencies
  include rvm
  rvm_system_ruby {
  "$ruby_version":
    ensure      => 'present',
    default_use => true,
    before => Exec["bundler"];
  }

  #### Apache Vhost and dependencies
  # Note: to avoid duplicate class declaration, we have forked puppetlabs-passenger
  # and removed the include from init.pp. This is necessary to provide params to
  # the apache module, otherwise apache is installed with all the defaults
  class { 'apache':
    default_mods           => false,
    default_confd_files    => false,
    mpm_module             => 'event',
  }

  # enable mod_headers
  apache::mod { 'headers': }

  # enable mime type checking
  apache::mod { 'mime': }
  apache::mod { 'mime_magic': }
  apache::custom_config { 'mime-types':
    content => 'TypesConfig /etc/mime.types',
  }

  # mod_dir
  apache::mod { 'dir': }

  # enable mod_deflate
  class { '::apache::mod::deflate':
    types => [ 'text/html',
               'text/plain',
               'text/xml',
               'text/css',
               'application/json',
               'application/xml',
               'application/x-javascript',
               'application/javascript',
               'application/ecmascript',
               'application/rss-xml',
               'application/font-woff',
             ],
  }



  # Assign directory and location blocks into local variables to dry out vhost block
  # Public <Direcotory />
  $vhost_directory = {
          path           => "$deploy_dir/current/public",
          allow_override => ['None'],
        }

  # Assets <Location />. Set far future cache control
  $vhost_location_assets = {
         path          => "/assets/",
         provider => 'location',
         headers => 'Set Cache-Control "public,max-age=31536000"',
       }

  # Root <Location /> with optional basic authentication
  if $auth_enabled {
    $vhost_location_root = {
         path          => "/",
         provider => 'location',
         auth_type     => 'Basic',
         auth_name     => 'Please login',
         auth_user_file => $auth_user_file,
         auth_require  => "user $auth_user_name",
       }
    # Gather up the various directory and location blocaks.
    $vhost_path_settings = [$vhost_directory, $vhost_location_root, $vhost_location_assets,]
  } else {
    # Gather up the various directory and location blocaks.
    $vhost_path_settings = [$vhost_directory, $vhost_location_assets,]
  }

  # Setup up the primary vhost
  apache::vhost { $apache_servername:
    port           => $apache_port,
    docroot        => "$deploy_dir/current/public",
    docroot_owner  => $deploy_user,
    directories    => $vhost_path_settings,
    serveraliases  => $apache_aliases,
    require        => [File['current'], Class['apache'],],
  }

  # Add a user file if we're using basic authentication and
  # enable necessary apache modules
  if $auth_enabled {
    # enable required apache modules
    apache::mod { 'auth_basic': }
    apache::mod { 'authn_core': }
    apache::mod { 'authn_file': }
    apache::mod { 'authz_user': }

    httpauth { $auth_user_name:
      file     => $auth_user_file,
      password => $auth_user_pass,
      ensure => present,
      require      => [File['current'], Apache::Vhost[$apache_servername]],
    }->
    file { $auth_user_file:
      mode => 644
    }
  }

  #### Capistrano
  include capistrano
  capistrano::deploytarget {'nli-pr':
    deploy_user    => $deploy_user,
    share_group    => $deploy_group,
    deploy_dir     => $deploy_dir,                 # default
    shared_dirs    => ['bin', 'bundle', 'config', 'public', 'config/initializers'], # default
  }

  file {'current':
    ensure      => link,
    path         => "$deploy_dir/current",
    target       => "$deploy_dir/releases/19700101000000",
    replace     => 'false',
    require      => File["blank_release"]
  }

  file {'blank_release':
    ensure       => directory,
    owner          => $deploy_user,
    path          => "$deploy_dir/releases/19700101000000",
    require      => Class["capistrano"]
  }

  # bundler needed by capistrano.
  # needs to be installed after rvm and via its own bash shell.
  # to be aware of rvm/ruby/gem settings.
  exec {'bundler':
    command => "bash -c 'source /etc/profile; gem install bundler'",
    logoutput => 'true',
    path => 'usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin',
    creates => "/usr/local/rvm/gems/$ruby_version/bin/bundler",
    require => Class['rvm'],
  }

  # git needed by capistrano
  include git

  # config files for the app
  file { 'secrets.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path    => "$deploy_dir/shared/config/secrets.yml",
    content => template('nli_pr/secrets_yml.erb'),
    require => Class["capistrano"]
  }
  file { 'solr.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/solr.yml",
    content => template('nli_pr/solr_yml.erb'),
    require => Class["capistrano"]
  }
  file { 'config.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/config.yml",
    content => template('nli_pr/config_yml.erb'),
    require => Class["capistrano"]
  }
  file { 'restricted.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/restricted.yml",
    content => template('nli_pr/restricted_yml.erb'),
    require => Class["capistrano"]
  }

  file { 'cache_config':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path    => "$deploy_dir/shared/config/initializers/cache_config.rb",
    content => template('nli_pr/cache_config.rb.erb'),
    require => Class["capistrano"]
  }

}

