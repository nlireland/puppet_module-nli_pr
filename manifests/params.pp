class nli_pr::params {
  $ruby_version = 'ruby-2.1.5'
  $deploy_dir = "/var/www/nli-pr"
  $deploy_user = "puppetadmin"
  $deploy_group = "puppetadmin"
  $blacklight_secret = "REPLACE"
  $solr_host = "127.0.0.1"
  $solr_port = "8080"
  $solr_cores = ["parish_registers"]
  $pr_mapbox_access_token = "REPLACE"
  $pr_mapbox_county_map = "REPLACE"
  $pr_mapbox_diocese_map = "REPLACE"
  $pr_mapbox_city_map = "REPLACE"
  $pr_local_county_map = "http://localhost:8888/v2/county_map.json"
  $pr_local_diocese_map = "http://localhost:8888/v2/diocese_map.json"
  $pr_local_city_map = "http://localhost:8888/v2/city_map.json"
  $pr_map_server_type = "local"
  $pr_image_server_ip = "localhost"
  $pr_image_server_fcgi_path = "/cgi-bin/iipsrv.fcgi"
  $pr_divaserve_ip = "localhost"
  $pr_divaserve_path = "/divaserve.php"
  $pr_image_url_path = "http://catalogue.nli.ie/cgi-bin/iipsrv.fcgi?FIF="
  $pr_use_static_jpgs = false
  $pr_feedback_email = "REPLACE IN HIERA"
  $pr_perform_caching = false
  $pr_cache_store     = 'UNDEF'
  $auth_enabled = false
  $auth_user_file = '/etc/httpd/auth_user_file'
  $auth_user_name = 'REPLACE IN HIERA'
  $auth_user_pass = 'REPLACE IN HIERA'
  $apache_servername = 'localhost'
  $apache_aliases = []
  $apache_port = '80'
}
